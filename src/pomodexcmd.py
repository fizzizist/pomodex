import cmd
from src.utils import error_print

def parse_positive_int(arg):
    try:
        arg = int(arg)
        if arg <= 0:
            raise PomodexCmd.CmdException
        return arg
    except ValueError as e:
        error_print('The input argument must be a valid integer')
        raise PomodexCmd.CmdException from e 


class PomodexCmd(cmd.Cmd):
    prompt = 'pomodex> '

    class CmdException(Exception):
        pass

    def __init__(self, pomodex):
        self.pomodex = pomodex
        self.issues = pomodex.list_issues()
        super().__init__()

    def postcmd(self, stop, line):
        self.issues = self.pomodex.list_issues()

    def do_exit(self, arg):
        'Exits the program: EXIT'
        exit()

    def do_break(self, arg):
        'Initiate a break with the number of minutes: BREAK 10'
        try:
            self.pomodex.take_break(parse_positive_int(arg))
        except self.CmdException:
            return

    def do_refresh(self, arg):
        'Refresh the JIRA issues displayed: REFRESH'
        # Nothing actually happens here because we just need postcmd to run again.

    def do_work(self, arg):
        'Work on a task ID for a certain number of minutes: WORK 19221 45'
        args = arg.split()
        if len(args) != 2:
            error_print('WORK command required 2 arguments. Use "help work" for more details.')

        try:
            args[1] = parse_positive_int(args[1])
        except self.CmdException:
            return

        try:
            self.pomodex.work_on(self.issues[args[0]], args[1])
        except KeyError:
            error_print("Error: The issue_id you entered does not correspond to an issue in the list.")

    def default(self, line):
        error_print('Unrecognized Command.')
