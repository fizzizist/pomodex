import os
import time
import sys
import select
import requests
import json

from typing import Dict
from tqdm import tqdm
from datetime import datetime
from time import sleep
from requests.auth import HTTPBasicAuth
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

from src.utils import get_formatted_time, error_print


class Pomodex:

    def __init__(self, config):
        self.jira_url = f'https://{config["jira_domain"]}.atlassian.net'
        self.jira_email = config['jira_email']
        self.jira_token = config['jira_token']
        self.sec_count = 0
        self.slack_user_client = WebClient(token=config['slack_user_token'])
        self.slack_bot_client = WebClient(token=config['slack_bot_token'])
        self.slack_id = self.get_slack_id(config['slack_name'])

    def get_slack_id(self, name):
        """Sets the users slack_id so that we can notify them when the timer is done."""
        try:
            data = self.slack_user_client.users_list().data
        except SlackApiError as e:
            error_print('Error: Slack API call for assigning user ID failed. Notificiations disabled.')
            error_print(f'Error details: {e}')
        for user in data['members']:
            if user.get('real_name') == name:
                return user['id']
        error_print(f'Error: Unable to set slack ID with name {name}. Notifications disabled.')

    def _jira_get_request(self, endpoint: str, params: Dict[str, str]):
        """Make a GET request to JIRA"""
        url = os.path.join(self.jira_url, endpoint)
        headers = {'Accept': 'application/json'}
        auth = HTTPBasicAuth(self.jira_email, self.jira_token)
        resp = requests.get(url, headers=headers, params=params, auth=auth)
        if not resp.status_code == 200:
            error_print(f'Error: The JIRA request to {endpoint} failed with status code {resp.status_code}\n')
            print(resp.json())
            exit()
        return resp

    def _jira_post_request(self, endpoint, data):
        """Sends a POST request to the JIRA API"""
        url = os.path.join(self.jira_url, endpoint)
        headers = {
            "Accept": "application/json",
            "Content-Type": 'application/json'
        }
        auth = HTTPBasicAuth(self.jira_email, self.jira_token)
        response = requests.post(url, data=data, headers=headers, auth=auth)
        if not response.status_code in {200, 201}:
            error_print(f'Error: The JIRA POST request to {endpoint} with data {data} '
                        f'failed with status code {response.status_code}.')
            print(response.json())
        
    def list_issues(self) -> Dict[str, any]:
        """List JIRA issues assigned to the user"""
        print('Available JIRA issues:')
        query = {'jql': f'assignee = "{self.jira_email}" AND status = 3'}
        response = self._jira_get_request('rest/api/3/search', query)
        issues = response.json()['issues']
        for issue in issues:
            print(f'{issue["id"]}: {issue["key"]} {issue["fields"]["summary"]}')
        return {issue['id']: issue for issue in issues}

    def set_slack_status(self, status, emoji, timeEnd):
        """Sets the slack status of the user."""
        # Cannot set slack status if the status text is longer than 100 characters
        if len(status) > 100:
            status = f'{status[:97]}...'
        profile = {
            "status_text": status,
            "status_emoji": emoji,
            "status_expiration": timeEnd
        }
        try:
            self.slack_user_client.users_profile_set(profile=profile)
        except SlackApiError as e:
            error_print('Error: Slack status update failed.')
            error_print(f'Error defail: {e}')

    def notify_slack(self, msg):
        """sends a message to the slackbot to notify the user."""
        # Play a basic terminal sound as well
        os.system('echo "\007"')
        if self.slack_id:
            try:
                self.slack_bot_client.chat_postMessage(channel=self.slack_id, text=msg)
            except SlackApiError as e:
                error_print(f"Error while sending slack notification: {e}")

    def work_log(self, issue_id, time_start):
        """Logs time worked to the JIRA ticket"""
        payload = json.dumps({
            "timeSpentSeconds": self.sec_count,
            "comment": {
                "type": "doc",
                "version": 1,
                "content": [
                    {
                        "type": "paragraph",
                        "content": [
                            {
                                "text": "Work logged from Pomodex app.",
                                "type": "text"
                            }
                        ]
                    }
                ]
            },
            "started": time_start
        })
        self._jira_post_request(f'rest/api/3/issue/{issue_id}/worklog', payload)

    def run_timer(self, mins):
        """Creates and runs the timer thread."""
        try:
            secs = int(mins) * 60
        except ValueError:
            error_print("Error: the time entered needs to be an integer.")
            return

        print(f'Timer set for {get_formatted_time(secs)}. Use ctrl+C to stop early.')

        self.sec_count = 0
        try:
            for _ in tqdm(range(secs), ascii=True, dynamic_ncols=True, colour='green',
                          bar_format='{l_bar}{bar}| [{elapsed}<{remaining}] '):
                sleep(1)
                self.sec_count += 1
        except KeyboardInterrupt:
            print(f'Timer stopped after {get_formatted_time(self.sec_count)}')
        
    def work_on(self, issue, mins):
        """Sets a timer for a specific issue and then logs the time worked"""
        print(f'Working on issue {issue["key"]}')
        time_start = datetime.now()
        try:
            for_str = f' for {mins} minutes'
            work_str = f'Working on {issue["key"]} {issue["fields"]["summary"]}'
            # Cannot set slack status if the status_text exceeds a length of 100 characters
            if (len(for_str) + len(work_str)) > 100:
                new_len = 100 - (len(for_str) + 3)
                work_str = f'{work_str[:new_len]}...'
            self.set_slack_status(f'{work_str}{for_str}', ':thinking_face:', time.time() + (int(mins) * 60))
        except ValueError:
            error_print("Error: the time entered need to be an integer")
        
        self.run_timer(mins)

        self.set_slack_status('', '', 0)
        self.notify_slack(f'Pomodex timer stopped after {get_formatted_time(self.sec_count)}.')
        if self.sec_count >= 60:
            self.work_log(issue['id'], time_start.strftime('%Y-%m-%dT%H:%M:%S.000+0000'))
        else:
            print("You did not work for more than 60 seconds, so no time will be logged on the JIRA issue.")

    def take_break(self, break_mins):
        self.run_timer(break_mins)

        msg = f'Your break is over. Time to get back to work.'
        print(msg)
        self.notify_slack(msg)

