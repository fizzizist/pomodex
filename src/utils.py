def get_formatted_time(secs):
    """Formats seconds into hour, minute, seconds string"""
    mins, secs = divmod(secs, 60)
    hours, mins = divmod(mins, 60)
    ret = ''
    if hours > 0:
        ret = f'{hours}h '
    if mins > 0 or hours > 0:
        ret += f'{mins}m '
    return ret + f'{secs}s'

def error_print(msg):
    """Turns message red and prints it."""
    error_colo = '\033[91m'
    endc = '\033[0m'
    print(f'{error_colo}{msg}{endc}')
