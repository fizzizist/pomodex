# Pomodex

A lightweight command-line app for JIRA task tracking that changes your slack status when using the timer.

## Installation

### Setup
Install python venv and requirements, and run the program.
```python
python3 -m venv venv
source venv/bin/activate
pip install pip-tools
pip-compile
pip-sync
python main.py
```

You will need a few things to configure the program

* Slack User Token: xoxp-rest-of-token
* Slack Bot Token: xoxb-rest-of-token
* Slack name: your display name on Slack (used to send you notification messages)
* JIRA Domain: mydomain
* JIRA email: example@jiraemail.com
* JIRA Token: (token from JIRA settings)

This assumes that you have already set up a slack bot and have a user slack token beginning in `xoxp` and a bot token beginning in `xoxb`. The slack bot should have at least the following permission scopes: User scopes: `users.profile:write`, `users:read`. Bot scopes: `chat:write`. 
