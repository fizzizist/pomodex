import os
import yaml

from getpass import getpass

from src import Pomodex, PomodexCmd
from src.utils import error_print


def load_config():
    """Function to load in the config file, or create one"""
    if not os.path.exists('config.yml'):
        print("No config found. Let's configure the app.")
        jira_email = input('Enter your JIRA email: ')
        jira_domain = input('Enter your JIRA domain: ')
        jira_token = getpass("Enter your JIRA API token: ")
        slack_user_token = getpass("Enter your slack user token (beginning with xoxp): ")
        slack_bot_token = getpass("Enter your slack bot token (beginning with xoxb): ")
        slack_name = input("Slack display name (for getting your user ID to send notifications): ") 

        config = {'jira_email': jira_email,
                  'jira_token': jira_token,
                  'jira_domain': jira_domain,
                  'slack_user_token': slack_user_token,
                  'slack_bot_token': slack_bot_token,
                  'slack_name': slack_name}

        with open('config.yml', 'w') as out_file:
            yaml.dump(config, out_file)

        return config

    with open('config.yml', 'r') as yml_file:
        config = yaml.safe_load(yml_file)
            
    try:
        assert 'jira_email' in config
        assert 'jira_token' in config
        assert 'jira_domain' in config
        assert 'slack_user_token' in config
        assert 'slack_bot_token' in config
        assert 'slack_name' in config
    except AssertionError:
        error_print('Error: Config file is malformed. Please delete the current config.yml file and restart the program.')
        exit()

    return config        


def main():
    """Main app function. The starting point."""
    print("Welcome to Pomodex. A command-line tool for keeping yourself busy!")
    print("Loading in config.")
    config = load_config()

    pomodex = Pomodex(config)
    
    PomodexCmd(pomodex).cmdloop()

main()
